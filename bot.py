#!/home/arkady/venv/bin/python

import asyncio
import logging
import random
from datetime import datetime

import pyppeteer
from peewee import fn

import settings
from main import wait
from models import create_db, Wine, Producer, BotRating, BotUser


async def with_user_agent(page):
    user_agent = random.choice(settings.DEFAULT_USER_AGENTS)
    await page.setUserAgent(user_agent)

    return page


async def get_random_wine(db):
    wine = await db.execute(
        Wine.select()
            .join(Producer, on=(Producer.id == Wine.producer_id))
            .where(Wine.vivino_url != "N.V.",
                   Wine.vivino_url != None,
                   Wine.vintage >= datetime.now().year - 6,
                   Producer.name != "Hauksson")
            .order_by(fn.Random()).limit(1)
    )

    for w in wine:
        return w


async def life_iteration(db, browser, bot_user):
    random_wine = await get_random_wine(db)
    page = await with_user_agent(await browser.newPage())
    try:
        page = await login(page, db, bot_user)

        if not page:
            logging.info('exit because not login_elem and password_elem')
            return

        avg_rating, page = await get_avg_rating(page, random_wine.vivino_url)

        await add_comment(page, db, avg_rating <= 3.55)

    finally:
        await page.close()


async def login(page, db, bot_user):
    url = "https://www.vivino.com/login"
    await page.goto(url, {"waitUntil": "networkidle2"})

    login_elem = await page.querySelector('#logins-email-field')
    password_elem = await page.querySelector('#password')

    if login_elem and password_elem:
        logging.info('update last time activity for: %s', bot_user.login)
        asyncio.ensure_future(
            bot_user.update_last_time_activity(db)
        )

        await login_elem.type(bot_user.login)
        await password_elem.type(bot_user.password)

        await asyncio.gather(
            page.click("#form-btn"),
            page.waitForNavigation(),
        )

        return page


async def get_avg_rating(page, url):
    await page.goto(url, {"waitUntil": "networkidle2"})

    raw_avg = await page.xpath('//*[@id="vintage-page-app"]/div[1]/div/div/div[1]/div/div[2]/div[2]/a/div/div/span')

    if len(raw_avg) == 0:
        return 0.0, page

    rating = await page.evaluate('(element) => element.textContent', raw_avg[0])
    return float(rating), page


async def get_comment(db, bad=False):
    type_rating = "bad" if bad else "good"

    rating = await db.get(
        BotRating.select()
            .order_by(fn.Random())
            .where(BotRating.type_rating == type_rating)
    )
    return rating


async def add_comment(page, db, bad):
    comment = await get_comment(db, bad)
    await wait(page, '#all_reviews')
    await asyncio.sleep(2)
    list_a = await page.xpath('//*[@id="vintage-page-app"]/div[2]/div/div[3]/div[2]/div/div/div[1]//*')

    for a in list_a:
        val = await page.evaluate('(element) => element.getAttribute("data-value")', a)
        if not val:
            continue

        if comment.value == float(val):
            selected_a = a
            break

    else:
        return

    await selected_a.click()

    logging.info('set rating %s for vine %s', comment.text, page.url)

    textarea = (await page.xpath('//*[@id="vintage-page-app"]/div[2]/div/div[3]/div[2]/div/div/textarea'))[0]
    await textarea.type(comment.text)

    await asyncio.sleep(1)

    await page.click("#submitAnchor")

    await asyncio.sleep(2)


async def start():
    # create db pool
    db = create_db()

    try:
        bot_users = await db.execute(BotUser.select())

        for bot_user in bot_users:
            browser = await pyppeteer.launch(headless=True)
            try:
                logging.info('start bot for user: %s', bot_user.login)
                await life_iteration(db, browser, bot_user)
            except Exception as e:
                logging.error(e, exc_info=True)
            finally:
                logging.info("done for: %s", bot_user.login)
                await browser.close()

                logging.info(" start sleep 5 minutes...")
                await asyncio.sleep(5 * 60)
    finally:
        await db.close()


def main():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s:%(levelname)s:%(message)s",
        handlers=[
            logging.FileHandler("logs-bot.log"),
            logging.StreamHandler()
        ])

    loop = asyncio.get_event_loop()
    loop.run_until_complete(start())


if __name__ == '__main__':
    main()
