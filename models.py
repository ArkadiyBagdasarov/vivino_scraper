import datetime
import time

import peewee
import peewee_async

from settings import pg_db_config

database = peewee_async.PooledPostgresqlDatabase(**pg_db_config)


def create_db():
    database.set_allow_sync(False)
    return peewee_async.Manager(database)


class BaseModel(peewee.Model):
    class Meta:
        database = database


class Person(BaseModel):
    name = peewee.TextField()
    country = peewee.TextField()
    website = peewee.TextField(null=True)
    vivino_url = peewee.TextField()
    track = peewee.BooleanField(default=False)

    class Meta:
        db_table = 'persons'


class PersonUpdate(BaseModel):
    person = peewee.ForeignKeyField(Person)
    ratings = peewee.IntegerField()
    rank_in_country = peewee.IntegerField()
    followers = peewee.IntegerField()
    following = peewee.IntegerField()
    timestamp = peewee.DateTimeField(default=datetime.datetime.now)

    class Meta:
        db_table = 'persons_update'


class Producer(BaseModel):
    vivino_id = peewee.IntegerField(null=True)
    name = peewee.TextField(null=True)
    country = peewee.TextField(null=True)
    region = peewee.TextField(null=True)
    vivino_url = peewee.TextField()
    track = peewee.BooleanField(default=False)

    class Meta:
        db_table = 'producers'


class Wine(BaseModel):
    producer = peewee.ForeignKeyField(Producer)
    name = peewee.TextField(null=True)

    vivino_id = peewee.IntegerField()
    region = peewee.TextField(null=True)
    variety = peewee.TextField(null=True)
    vintage = peewee.TextField(null=True)
    vivino_url = peewee.TextField(null=True)

    class Meta:
        db_table = 'wines'

    @staticmethod
    async def create_wine(db, producer_id, **kwargs):
        w, create = await db.get_or_create(
            Wine,
            producer_id=producer_id,
            vivino_id=kwargs.pop('vivino_id'),
            vintage=kwargs.pop('vintage')
        )

        if create:
            for k, v in kwargs.items():
                setattr(w, k, v)

            await db.update(w, tuple(kwargs.keys()))

        return w


class Ratings(BaseModel):
    wine = peewee.ForeignKeyField(Wine)
    person = peewee.ForeignKeyField(Person)

    comment = peewee.TextField(null=True)
    rating = peewee.FloatField(null=True)
    price = peewee.FloatField(null=True)
    timestamp = peewee.DateTimeField(null=True)

    @staticmethod
    async def create_rating(db, person_id, wine_id, comment, rating, price, timestamp):
        r, _ = await db.get_or_create(
            Ratings, person_id=person_id,
            wine_id=wine_id
        )
        r.comment = comment
        r.rating = rating
        r.price = price
        r.timestamp = timestamp

        await db.update(r, ('comment', 'rating', 'price', 'timestamp'))
        return r


class BotUser(BaseModel):
    login = peewee.TextField()
    password = peewee.TextField()
    last_time_activity = peewee.TimestampField(utc=True, default=time.time)

    async def update_last_time_activity(self, db):
        self.last_time_activity = time.time()

        await db.update(self, ('last_time_activity',))


class BotRating(BaseModel):
    type_rating = peewee.CharField(max_length=20)
    value = peewee.FloatField()
    text = peewee.TextField()
