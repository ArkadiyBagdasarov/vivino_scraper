import json
import random
import datetime

import aiohttp
import asyncio
import logging

import pyppeteer
import pytz
from bs4 import BeautifulSoup
from dateutil.parser import parse
from pyquery import PyQuery as pq
from pyppeteer.errors import ElementHandleError

import settings
from models import create_db, Producer, Wine, Ratings, Person, PersonUpdate


async def wait(page, selector):
    try:
        await page.waitForSelector(selector)
    except pyppeteer.errors.TimeoutError:
        return False
    else:
        return True


def get_float(value):
    data = ''.join(i for i in value if i.isdigit() or i == ',')

    if not data:
        return 0.0

    return float(data.replace(',', '.'))


async def do_request(url, is_json=False, if_error=None):
    await asyncio.sleep(5.2)

    async with aiohttp.ClientSession(headers=settings.headers, timeout=aiohttp.ClientTimeout(total=10)) as session:
        try:
            async with session.get(url) as resp:
                text = await resp.text()
        except asyncio.TimeoutError:
            return if_error

    if is_json:
        try:
            body = json.loads(text)
        except json.decoder.JSONDecodeError:
            await asyncio.sleep(5.2)
            return if_error
        else:
            return body

    return text


async def get_wines(id_producer):
    url = settings.API_URL + "wineries/{id_producer}/wines?start_from=0&limit=1000&sort=most_rated&include_all_vintages=true".format(
        id_producer=id_producer)

    body = await do_request(url, is_json=True, if_error=[])

    if not body:
        return body

    return body['wines']


async def get_price(id_wine):
    url = settings.API_URL + "prices?vintage_ids[]={}".format(id_wine)

    body = await do_request(url, is_json=True, if_error={})

    if body:
        if body.get('prices'):
            return body['prices']['vintages'][str(id_wine)]

    return {}


async def get_grapes(id_wine):
    url = settings.API_URL + "vintages/{}/vintage_page_information".format(id_wine)

    body = await do_request(url, is_json=True, if_error='')

    if not body:
        return body

    grapes = body['vintage_page_information']['vintage']['wine']['grapes']

    return ', '.join(g['name'] for g in grapes)


async def get_wine_info(id_wine):
    url = settings.API_URL + "vintages/{}/vintage_page_information".format(id_wine)

    body = await do_request(url, is_json=True, if_error={})

    if not body:
        return body

    vintage = body['vintage_page_information']['vintage']
    wine = vintage['wine']

    grapes = ', '.join(g['name'] for g in wine['grapes'])

    kwargs = dict(
        vintage=vintage['year'],
        variety=grapes,
        region=wine['region']['name'] if wine['region'] else '',
        vivino_url='https://www.vivino.com/{}/w/{}?year={}'.format(
            '{}-{}'.format(wine['winery']['seo_name'], wine['seo_name']),
            wine['id'], vintage['year']),
        name=vintage['name'],
        vivino_id=wine['id']

    )

    return kwargs, vintage['statistics']['ratings_average']


async def create_vintage(db, wine, vintage, producer_id):
    grapes = await get_grapes(vintage['id'])

    kwargs = dict(
        vintage=vintage['year'],
        variety=grapes,
        region=wine['region']['name'] if wine['region'] else '',
        vivino_url='https://www.vivino.com/{}/w/{}?year={}'.format(
            '{}-{}'.format(wine['winery']['seo_name'], wine['seo_name']), wine['id'], vintage['year'])
    )
    w, create = await db.get_or_create(
        Wine,
        name=vintage['name'],
        producer_id=producer_id,
        vivino_id=wine['id']
    )

    if create:
        for k, v in kwargs.items():
            setattr(w, k, v)

        await db.update(w, tuple(kwargs.keys()))

    return w


async def add_wines(db, producer, wines):
    wines_db = []
    for wine in wines:
        for vintage in wine['vintages']:

            if not vintage['year']:
                continue

            wines_db.append(await create_vintage(db, wine, vintage, producer.id))

    return wines_db


async def get_rewiews(wine, page):
    url = settings.API_URL + "wines/{}/reviews?page={}".format(wine.vivino_id, page)

    body = await do_request(url, is_json=True, if_error=[])

    if not body:
        return body

    return body['reviews']


async def add_rewiews(db, reviews, wine, browser):
    for review in reviews:
        page = await browser.newPage()

        try:
            user_agent = random.choice(settings.DEFAULT_USER_AGENTS)
            await page.setUserAgent(user_agent)

            url = "https://www.vivino.com/users/{}".format(review['user']['seo_name'])
            await page.goto(url, {"waitUntil": "networkidle2"})

            person = await add_person(db, page)
            if not person:
                continue

            try:
                last_rating = await db.get(
                    Ratings.select()
                        .where(Ratings.person_id == person.id)
                        .order_by(Ratings.timestamp.desc())
                )
            except Ratings.DoesNotExist:
                last_rating = None

            price = await get_price(wine.vivino_id)
            await Ratings.create_rating(
                db, person_id=person.id, wine_id=wine.id,
                comment=review['note'], rating=review['rating'],
                price=price['price']['amount'] if price else 0.0,
                timestamp=parse(review['created_at'])
            )

            await scroll_to_last_rating(page, last_rating)

            page_source = pq(await page.content())

            kwargs = {"person_id": person.id}

            for feedback in page_source.items('div.user-activity-item'):
                producer_link = feedback.find("span.text-small a.link-muted").attr('href')

                if not producer_link:
                    continue

                timestamp = parse(feedback.find("a.link-muted.bold.inflate").attr('title'))
                now = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)

                if timestamp.timestamp() > now.timestamp():
                    timestamp = timestamp - datetime.timedelta(days=365)

                if last_rating:
                    if timestamp.timestamp() < last_rating.timestamp.replace(tzinfo=pytz.UTC).timestamp():
                        break

                kwargs['timestamp'] = timestamp

                producer = await get_producer(db, 'https://www.vivino.com' + producer_link)

                kwargs['comment'] = feedback.find("p.tasting-note.text-larger").text().strip('"')
                kwargs['price'] = get_float(feedback.find('span.wine-price-value').text())

                new_wine_id = feedback.find("div.activity-section.activity-wine-card.clearfix").attr(
                    'data-vintage-id')

                wine_info, wine_rating_average = await get_wine_info(new_wine_id)
                kwargs['rating'] = wine_rating_average

                try:
                    feedback_wine = await db.get(
                        Wine,
                        vivino_id=wine_info['vivino_id'],
                        vintage=wine_info['vintage']
                    )
                except Wine.DoesNotExist:
                    feedback_wine = await Wine.create_wine(
                        db, producer_id=producer.id, **wine_info
                    )

                kwargs['wine_id'] = feedback_wine.id

                await Ratings.create_rating(db, **kwargs)
                logging.info('create Ratings: %s', kwargs)
        except Exception as e:
            logging.error(e, exc_info=True)

        finally:
            await page.close()


async def add_person(db, page):
    person_data = {}

    page_source = pq(await page.content())

    name = page_source.find('div.user-header__name.header-medium.bold').text()

    if not name:
        logging.warning("most likely you were banned\n"
                        "(can not get person info)")
        return None

    person_data['name'] = page_source.find('div.user-header__name.header-medium.bold').text()

    country_raw = page_source.find('div.user-header__status.text-mini.text-muted')
    person_data['website'] = page_source.find('div.user-header__status.text-mini.text-muted a').text()
    country_raw.remove('a.text-muted')
    person_data['country'] = country_raw.text()

    person_data['vivino_url'] = page.url

    person, _ = await db.get_or_create(Person, **person_data)

    person_update_data = {'person_id': person.id}

    person_update_data['ratings'] = page_source.find(
        'div.col-xs-4.col-xs-offset-2.text-center span.header-large.light').text()

    person_update_data['rank_in_country'] = page_source.find(
        'div.row-no-gutter.user-overview.text-left div.col-xs-4.text-center a').text()

    followers_and_following = []
    for f in page_source.items("a.text-giga.bold.link-muted"):
        followers_and_following.append(f.text())

    person_update_data['followers'] = followers_and_following[0]
    person_update_data['following'] = followers_and_following[1]

    await db.create(PersonUpdate, **person_update_data)

    return person


async def get_producer(db, url):
    body = await do_request(url, is_json=False, if_error='')

    soup = BeautifulSoup(body, 'lxml')

    name = soup.select_one("h1.content-page-header__headline").text

    location = soup.select_one("div.location")
    if location:
        country_and_region = [i.text for i in location.find_all("a")]
    else:
        country_and_region = ['', '']

    content = soup.find("meta", property='al:android:url')['content']

    vivino_id = ''.join([i for i in content if i.isdigit()])

    producer, _ = await db.get_or_create(
        Producer,
        vivino_url=url
    )

    producer.vivino_id = vivino_id
    producer.name = name
    producer.country = country_and_region[0]
    producer.region = country_and_region[1]

    await db.update(producer, ('vivino_id', 'name', 'country', 'region'))

    return producer


async def scroll_to_last_rating(page, last_rating):
    prev_count = 0

    for _ in range(10):
        # max 10 iterations, but if we find feedback witch timestamp less that
        # timestamp from last rating - we will return from this function

        if not await wait(page, "#btn-more-activities"):
            continue

        page_source = pq(await page.content())

        for feedback in page_source.items('div.user-activity-item'):
            timestamp = parse(feedback.find("a.link-muted.bold.inflate").attr('title'))
            now = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)

            if timestamp.timestamp() > now.timestamp():
                timestamp = timestamp - datetime.timedelta(days=365)

            if last_rating:
                if timestamp.timestamp() < last_rating.timestamp.replace(tzinfo=pytz.UTC).timestamp():
                    return

        await page.evaluate('document.getElementById("btn-more-activities").click();')
        await asyncio.sleep(1)

        await page.keyboard.down("End")
        await asyncio.sleep(5.2)

        if not await wait(page, "div.activity-card"):
            continue

        cur_count = await page.evaluate('document.getElementsByClassName("activity-card").length;')

        if cur_count == prev_count:
            break

        prev_count = cur_count


async def start():
    # create db pool
    db = create_db()

    producers = await db.execute(
        Producer.select()
            .where(Producer.track == True)
    )
    for p in producers:
        producer = await get_producer(db, p.vivino_url)

        wines = await get_wines(producer.vivino_id)
        wines_db = await add_wines(db, producer, wines)

        for wine in wines_db:
            page = 1
            rewiews = []

            while True:
                data = await get_rewiews(wine, page)

                if not data:
                    break

                rewiews.extend(data)
                page += 1

            if rewiews:
                browser = await pyppeteer.launch(headless=True)
                try:
                    await add_rewiews(db, rewiews, wine, browser)
                finally:
                    await browser.close()


def main():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s:%(levelname)s:%(message)s",
        handlers=[
            logging.FileHandler("logs.log"),
            logging.StreamHandler()
        ])

    loop = asyncio.get_event_loop()
    loop.run_until_complete(start())


if __name__ == '__main__':
    main()
