import logging

import peewee

from models import database, Person, PersonUpdate, Producer, Wine, Ratings, BotRating, BotUser

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',
                    level=logging.INFO)

logger = logging.getLogger()

logger.info('INIT DB')
objects = peewee.PostgresqlDatabase(database)


def create_bot_ratings():
    with open('bad.txt', 'r') as f:
        for row in f.readlines():
            row = row.split(';')
            BotRating.get_or_create(
                type_rating='bad',
                value=row[0].replace(',', '.'),
                text=row[1].strip('\n')
            )

    with open('good.txt', 'r') as f:
        for row in f.readlines():
            row = row.split(';')
            BotRating.get_or_create(
                type_rating='good',
                value=row[0].replace(',', '.'),
                text=row[1].strip('\n')
            )


logger.info('START CREATE TABLES')
Person.create_table(True)
PersonUpdate.create_table(True)
Producer.create_table(True)
Wine.create_table(True)
Ratings.create_table(True)
BotRating.create_table(True)
BotUser.create_table(True)

create_bot_ratings()
logger.info('END CREATE TABLES')
