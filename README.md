# Install on clear server

## Install postgres:
* ```1. sudo apt-get update && sudo apt-get install postgresql postgresql-contrib -y```
* ```2. sudo su postgres```
* ```3. createdb vivino```
* ```4. psql vivino```
* ```5. EXECUTE: create user vivino with password 'ghbdtn';```
* ```6. EXECUTE: GRANT ALL PRIVILEGES ON DATABASE vivino to vivino;```

## Install app:
* ```1. sudo apt install virtualenv```
* ```2. virtualenv -p python3 venv```
* ```3. source venv/bin/activate```
* ```4. git clone https://ArkadiyBagdasarov@bitbucket.org/haukssonh/scraper.git```
* ```5. pip install -r scraper/req.txt```
* ```6. python scraper/migrations.py```
* ```7. python scraper/main.py```


# Run scraper

* ```1. cd /home/arkady```
* ```2. source venv/bin/activate```
* ```3. python scraper/main.py```